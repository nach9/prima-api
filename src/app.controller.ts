import {
  Controller,
  Request,
  Post,
  UseGuards,
  Body,
  Put,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { UpdateUserPasswordDto } from './users/dto/update-user-password.dto';
import { UsersService } from './users/users.service';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginUserDto } from './auth/login.dto';

@UseInterceptors(ClassSerializerInterceptor)
@ApiTags('Auth')
@Controller()
export class AppController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @ApiOperation({
    summary: 'Login User',
    description: 'For user login using email and password',
  })
  @ApiBody({ type: LoginUserDto })
  @Post('api/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @ApiOperation({
    summary: 'Update Password',
    description: 'For edit/update current user (based on token)',
  })
  @Put('api/password')
  async updatePassword(
    @Request() req,
    @Body() updateUserPassword: UpdateUserPasswordDto,
  ) {
    console.log(req.user);
    return this.usersService.updatePassword(req.user.id, updateUserPassword);
  }
}
