import { Exclude } from 'class-transformer';

export class UserVM {
  constructor(partial: Partial<UserVM>) {
    Object.assign(this, partial);
  }

  name: string;

  email: string;

  @Exclude()
  password: string;

  role: string;
}
