import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsIn, IsString, Matches } from 'class-validator';
import { UserRole } from '../entities/user.entity';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty({
    description:
      'password min 8 chars, contains uppercase, lowercase, number, and symbol',
  })
  @IsString()
  @Matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/, {
    message:
      'password min 8 chars, contains uppercase, lowercase, number, and symbol',
  })
  password: string;

  @ApiProperty({ enum: Object.keys(UserRole).map((k) => UserRole[k]) })
  @IsString()
  @IsIn(Object.keys(UserRole).map((k) => UserRole[k]))
  role: string;
}
