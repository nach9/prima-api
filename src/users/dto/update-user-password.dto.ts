import { ApiProperty } from '@nestjs/swagger';
import { IsString, Matches } from 'class-validator';

export class UpdateUserPasswordDto {
  @ApiProperty({
    description:
      'password min 8 chars, contains uppercase, lowercase, number, and symbol',
  })
  @IsString()
  @Matches(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/, {
    message:
      'password min 8 chars, contains uppercase, lowercase, number, and symbol',
  })
  password: string;
}
