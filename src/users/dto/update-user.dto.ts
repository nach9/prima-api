import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsString, IsEmail, IsOptional } from 'class-validator';
import { UserRole } from '../entities/user.entity';

export class UpdateUserDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({ enum: Object.keys(UserRole).map((k) => UserRole[k]) })
  @IsOptional()
  @IsString()
  @IsIn(Object.keys(UserRole).map((k) => UserRole[k]))
  role: string;
}
