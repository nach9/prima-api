import { HttpStatus, HttpException } from '@nestjs/common';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace UserException {
  export class NotFound extends HttpException {
    constructor() {
      super('User Not Found', HttpStatus.NOT_FOUND);
    }
  }

  export class EmailExisted extends HttpException {
    constructor(email: string) {
      super(`Email ${email} already existed`, HttpStatus.BAD_REQUEST);
    }
  }
}
