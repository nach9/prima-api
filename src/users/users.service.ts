import { Injectable, Logger } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User, UserRole } from './entities/user.entity';
import { UserException } from './exceptions/user.exception';
import * as bcrypt from 'bcrypt';
import { UserVM } from './vms/user.vms';
import { UpdateUserPasswordDto } from './dto/update-user-password.dto';

@Injectable()
export class UsersService {
  private readonly logger = new Logger(User.name);
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    this.logger.log('create user');
    const { role, email, password } = createUserDto;
    let newUser;
    try {
      newUser = await this.usersRepository.save({
        ...createUserDto,
        password: await bcrypt.hash(password, process.env.PASS_SALT),
        role: role as UserRole,
      });
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY')
        throw new UserException.EmailExisted(email);
    }

    return new UserVM(newUser);
  }

  async findAll() {
    this.logger.log('get user');
    const users = await this.usersRepository.find();
    return users.map((user) => new UserVM(user));
  }

  async findOne(id: number) {
    this.logger.log('get user by Id');
    const user = await this.usersRepository.findOneBy({ id });
    if (user === null) throw new UserException.NotFound();

    return new UserVM(user);
  }

  async findEmail(email: string) {
    this.logger.log('get user by email');
    const user = await this.usersRepository.findOneBy({ email });
    if (user === null) throw new UserException.NotFound();

    return new UserVM(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    this.logger.log('update user');
    const { role, email } = updateUserDto;
    const user = await this.usersRepository.findOneBy({ id });
    if (user === null) throw new UserException.NotFound();

    let updatedUser;

    try {
      updatedUser = await this.usersRepository.save({
        ...user,
        ...updateUserDto,
        role: role as UserRole,
      });
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY')
        throw new UserException.EmailExisted(email);
    }

    return new UserVM(updatedUser);
  }

  async remove(id: number) {
    this.logger.log('delete user');
    const user = await this.usersRepository.findOneBy({ id });
    if (user === null) throw new UserException.NotFound();

    await this.usersRepository.delete(id);

    return new UserVM(user);
  }

  async updatePassword(
    id: number,
    updateUserPasswordDto: UpdateUserPasswordDto,
  ) {
    this.logger.log('update password');
    const user = await this.usersRepository.findOneBy({ id });
    if (user === null) throw new UserException.NotFound();

    const updatedUser = await this.usersRepository.save({
      ...user,
      ...updateUserPasswordDto,
    });

    return new UserVM(updatedUser);
  }
}
