import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, Matches } from 'class-validator';

export class LoginUserDto {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty({
    description:
      'password min 8 chars, contains uppercase, lowercase, number, and symbol',
  })
  @IsString()
  password: string;
}
